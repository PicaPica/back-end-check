import React from 'react';
import '../styles/index.css';


var MainButton =(props)=>{
  
    return (
      <button
        className="button"
        onClick={props.handleClick}>{props.label}</button>
    );
  
}

export default MainButton;