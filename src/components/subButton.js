import React from 'react';
import '../styles/index.css';


var SubButton =(props)=>{
  
    return (
      <button
        className="button subButton"
        onClick={props.handleClick}>{props.label}</button>
    );
  
}

export default SubButton;