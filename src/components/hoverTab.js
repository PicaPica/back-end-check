import React, { useState } from 'react';
import Button from './MainButton';
import SubButton from "./subButton";
import '../styles/index.css';


const HoverTab=(props)=>{
  const Filter=(filterName)=>{
  props.setData(props.DataOrigin.filter((item)=>item.auditState === filterName) );
  props.setPage(0);
}
  const Reset=()=>{
  props.setData(props.DataOrigin);
  props.setPage(0);
}
    return(
    <div className="tab">
      <Button label={props.hoverTabName} handleClick={Reset}/>
      <SubButton label="未審核" handleClick={()=>{Filter('未審核')}}/>
      <SubButton label="審核完成" handleClick={()=>{Filter('已審核')}}/>
      <SubButton label="通過"/>
      <SubButton label="未通過"/>
    </div>
    );
 }
 export default HoverTab;