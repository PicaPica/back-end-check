import React, { useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableRows from '../components/tableRow';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import TableBody from '@material-ui/core/TableBody';
import {grey}  from '@material-ui/core/colors';
import '../styles/index.css';
const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor:grey[50],
    color: grey[800],
    fontWeight:700,
    fontFamily:'微軟正黑體',
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);
const useStyles = makeStyles(theme => ({
  root: {
    fontWeight:700,
    fontFamily:'微軟正黑體',
  
  },
  
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));




const PerformerTable=(props)=>{
 

  const classes = useStyles();
 
  
  const sortByDate=()=>{
  
  const sorted=[...props.data].sort((a,b)=>{
    a.Date=new Date(a.applyDate).getTime();
    b.Date=new Date(b.applyDate).getTime();
     return a.Date-b.Date;
     
  });
  props.setData(sorted);
  
};

const sortByName=()=>{
 
  const sortedName=[...props.data].sort((a,b)=>{
     return  a.auditState.localeCompare(b.auditState, 'zh-Hans-CN', {sensitivity: 'accent'});
     
  });
  props.setData(sortedName);
  
};







  return(
    
    <div className='dataFilter'>
    
    <Table >
    
      <TableHead>
        <TableRow>
              <StyledTableCell align="left" >審核狀態
              <IconButton onClick={sortByName} ><ExpandMoreIcon/></IconButton>
              </StyledTableCell>
              <StyledTableCell align="left">申請日期
              <IconButton onClick={sortByDate} ><ExpandMoreIcon/></IconButton>
              </StyledTableCell>
              <StyledTableCell align="left">表演者</StyledTableCell>
              <StyledTableCell align="left">表演者類別</StyledTableCell>
              <StyledTableCell align="left">表演名稱</StyledTableCell>
              <StyledTableCell align="left">
                <p>
                  第{props.page+1}頁/{Math.ceil(props.data.length / props.rowsPerPage) }頁
                  <IconButton onClick={()=>props.setPage(props.page-1)}
                              disabled={props.page === 0}
                              ><KeyboardArrowLeft /></IconButton>
                  <IconButton onClick={()=>props.setPage(props.page+1)}
                              disabled={props.page >= Math.ceil(props.data.length / props.rowsPerPage) - 1}
                              >
                             <KeyboardArrowRight /> </IconButton></p>
              </StyledTableCell>
        </TableRow>
      </TableHead>
      <TableBody>
          {(props.rowsPerPage > 0
            ? props.data.slice(props.page * props.rowsPerPage, props.page * props.rowsPerPage + props.rowsPerPage)
            : props.data
          ).map((performer)=>{
                
          return (
          <TableRows performer={performer}/>
           ); 
          })}
     </TableBody>      
    </Table>     
        
   
           
 </div>
 );
        }
 
 
 export default PerformerTable;