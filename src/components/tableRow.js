import React,{useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import sample from "../data/29fw7ky.jpg";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import {grey}  from '@material-ui/core/colors';

import '../styles/index.css';

const useStyles = makeStyles(theme => ({
    root: {
       color:grey[700],
    },
    input: {
        fontSize: "15px"
      },
}));   
const TableRows=props=>{

const classes = useStyles();
var auditClassName='auditstate';
const [open,setOpen]=useState(false);
const [passValue,setPassValue]=useState();
const[notPassReason,setNotPassReason]=useState();
const handlePassIsNot=event=>{
    setPassValue(event.target.value);
    console.log(passValue);
}
const handleOpen=()=>{
      setOpen(true);
      
}
const handleClose=()=>{

      setOpen(false);
}
return(

<TableRow {...(props.performer.auditState==='已審核')?auditClassName='auditstate':auditClassName='auditstate active'}>

    <TableCell>
        <p className={auditClassName}>{props.performer.auditState}</p>
    </TableCell>
    <TableCell>{props.performer.applyDate}
    </ TableCell>
    < TableCell>{props.performer.performer}</ TableCell>
    < TableCell>{props.performer.category}</ TableCell>
    < TableCell>{props.performer.performName}</ TableCell>
    < TableCell align="right"><IconButton onClick={handleOpen}><ExpandMoreIcon /></IconButton></ TableCell>
    <Dialog  maxWidth={"md"} open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="performerDetail">
            <TableRow {...(props.performer.auditState==='已審核')?auditClassName='auditstate':auditClassName='auditstate active'}>

               <TableCell>
                       <p className={auditClassName}>{props.performer.auditState}</p>
               </TableCell>
               <TableCell>{props.performer.applyDate}</ TableCell>
               <TableCell>{props.performer.performer}</ TableCell>
               <TableCell>{props.performer.category}</ TableCell>
               <TableCell>{props.performer.performName}</ TableCell>
            </TableRow>
        </DialogTitle>
    <DialogContent>
    <div className="dialogText"> 
    <Grid container spacing={1}>
         <Grid item xs={3}>
             <Paper elevation={0}>
                <p className="dialogTitle">表演者照片</p>
                <img src={sample} alt="sample" width='100%'></img>

             </Paper>
            
         </Grid>
         <Grid item xs={7}>
            <Paper elevation={0}>
            <p className="dialogTitle pinf">表演資訊</p>
            <Grid container spacing={2}>
                <Grid item xs >
                    <li>表演風格</li>
                    <p className='list'>123</p>
                    <li>表演費用</li>
                </Grid>
                <Grid item xs >
                    <li>表演長度</li>
                    <li>自有設備</li>
                </Grid>
                <Grid item xs >
                    <li>表演人數</li>
                    <li>所需設備</li>
                </Grid>
                <Grid item xs >
                   <li>車馬費</li>
                    <li>所需場地大小</li>
                </Grid>
                </Grid>
            </Paper>
         </Grid>
         <Grid item xs={2}>
            <Paper elevation={0}>
            <FormControl component="fieldset">
              <RadioGroup aria-label="pass" name="passForm" value={passValue} onChange={handlePassIsNot} >
             
               <FormControlLabel
                
                className={classes.root}
                value="pass"
                control={<Radio color="default" />}
                label="通過"
                />
               <FormControlLabel
               className={classes.root}
               value="notPass"
               control={<Radio  color="default"/>}
               label="未通過"
               />
               
              </RadioGroup>
              <TextField  id="reason" 
                          label="未通過原因" 
                          onChange={(e) => setNotPassReason(e.target.value)} 
                          value={notPassReason} 
                          variant="outlined" 
                          style={{marginTop:10}}
                          multiline
                          rowsMax="4"
                          InputProps={{ classes: { input:classes.input } }}
                         />
              
             </FormControl>
            </Paper>
         </Grid>
    </Grid>
    <Grid container spacing={1}>
         <Grid item xs={3}>
            <Paper elevation={0}>
                <p className="dialogTitle pinf">簡介</p>
                
            </Paper>
         </Grid>
         <Grid item xs >
            <Paper elevation={0}>
                <p className="dialogTitle pvideo">形象影片</p>
                <Grid container spacing={2}> 
                <Grid item xs ><img src={sample} alt="sample" width='90%'></img></Grid>
                <Grid item xs ><img src={sample}  alt="sample" width='90%'></img></Grid>
                </Grid>
            </Paper>
         </Grid>
        
    </Grid>
    <Grid container spacing={1}>
         <Grid item xs={3}>
             <Paper elevation={0}>
                <p className="dialogTitle pservice">其他服務提供</p>
                

             </Paper>
            
         </Grid>
         <Grid item xs={8}>
            
         </Grid>
         <Grid item xs={1}>
            <Paper elevation={0}>
                <p><button className="dialogTitle saveButton">確定儲存</button></p>
            </Paper>
         </Grid>
    </Grid>
    </div>
    </DialogContent>
      
    </Dialog>
    

</TableRow>

);

}



export default TableRows;