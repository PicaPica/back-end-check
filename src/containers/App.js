import React from 'react';
import '../styles/index.css';
import Header from '../components/header';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import PerformerCheck from '../containers/performerCheck'
function App() {
  const THEME = createMuiTheme({
    typography: {
      fontFamily: [
        'Nunito',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '微軟正黑體'
      ].join(','),
      
    }
 });
  return (
    <ThemeProvider theme={THEME}>
    <div className="page">
     <Header/>
     <PerformerCheck/>
 
    </div>
    </ThemeProvider>
  );
}

export default App;
