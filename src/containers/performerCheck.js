import React from 'react';
import  HoverTab from "../components/hoverTab";
import PerformerTable from "../components/performerTable";
import performerData from "../data/performerInf";
import { useState } from "react";

const PerformerCheck=()=>{
    const temp=Array.from(performerData);
    const [data,setData]=useState(temp);
    const [dataOrigin,setDataOrigin]=useState(temp);
    const handleData=(newData)=>{
      setData(newData);
   }
   const [page,setPage]=useState(0);
   const handlePage=(newPage)=>{
    setPage(newPage);
 }
   const [rowsPerPage,setRowsPerPage]=useState(20);
    return(
    <div>
      <HoverTab hoverTabName="表演者審核"  setData={handleData}  DataOrigin={dataOrigin} setPage={handlePage}/>
      <div className="tabContainer" >
        
      <PerformerTable data={data} setData={handleData} setPage={handlePage} page={page} rowsPerPage={rowsPerPage}/>
      </div>
    </div>
    );


 }
 export default PerformerCheck;